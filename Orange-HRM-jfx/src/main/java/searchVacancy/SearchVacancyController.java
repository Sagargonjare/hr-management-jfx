package searchVacancy;


import java.io.File;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import candidates.Candidates;
import users.User;
import vacancies.Vacancies;
import dashboard.Dashboard;
import dbOperation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import recrutement.Recrutement;
import searchCandidate.SearchCandidate;
import searchUser.Admin;

public class SearchVacancyController implements Initializable {
	@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	@FXML
	private TextField text;
	@FXML
	private Button back;

	public void back(ActionEvent event) {
		new Vacancies().show();
	}
	@FXML
	private TableView<Vacancyy> tableView;
	@FXML
	private TableColumn<Vacancyy,String> col1;
	@FXML
	private TableColumn<Vacancyy,String> col2;
	@FXML
	private TableColumn<Vacancyy,String> col3;
	@FXML
	private TableColumn<Vacancyy,String> col4;
	
	private ObservableList<Vacancyy> dataList=FXCollections.observableArrayList();
	
	
	public void admin(ActionEvent event) {
		new User().show();
	}
	public void recruitment(ActionEvent event) {
		new Recrutement().show();
	}
	public void dashboard(ActionEvent event) {
		new Dashboard().show();
	}
	

	private ObservableList<Vacancyy> data;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		data = FXCollections.observableArrayList();

		col1.setCellValueFactory(new PropertyValueFactory<Vacancyy, String>("userRole"));
		col2.setCellValueFactory(new PropertyValueFactory<Vacancyy, String>("employeeName"));
		col3.setCellValueFactory(new PropertyValueFactory<Vacancyy, String>("status"));
		col4.setCellValueFactory(new PropertyValueFactory<Vacancyy, String>("userName"));

		buildData();

		FilteredList<Vacancyy> filteredData = new FilteredList<>(data, b -> true);
		text.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(Vacancyy -> {
				if (newValue.isEmpty() || newValue.isBlank() || newValue == null) {
					return true;
				}
				String searchKeyword = newValue.toLowerCase();
				if (Vacancyy.getUserRole().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (Vacancyy.getEmployeeName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (Vacancyy.getStatus().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (Vacancyy.getUserName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				
				
				} else
					return false;

			});
		});

		SortedList<Vacancyy> sortedData = new SortedList<>(filteredData);

		sortedData.comparatorProperty().bind(tableView.comparatorProperty());
		tableView.setItems(sortedData);

	}

	public void buildData() {
		try {
			data = FXCollections.observableArrayList();
			String query = "Select*from recruVacancy";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				Vacancyy users = new Vacancyy();
				users.jobtitle.set(resultSet.getString(2));
				users.vacancy.set(resultSet.getString(3));
				users.status.set(resultSet.getString(4));
				users.manager.set(resultSet.getString(5));

				data.add(users);
			}
			tableView.setItems(data);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	}