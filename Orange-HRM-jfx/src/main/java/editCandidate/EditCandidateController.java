package editCandidate;

import java.net.URL;
import java.util.ResourceBundle;

import candidates.Candidates;
import dashboard.Dashboard;
import dbOperation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import recrutement.Recrutement;
import users.User;

public class EditCandidateController implements Initializable{
	@FXML
	private TextField candidateToEdit;
	@FXML
	private ComboBox vacancy;
	@FXML
	private TextField first;
	@FXML
	private TextField last;
	@FXML
	private TextField email;
	@FXML
	private TextField number;
	@FXML
	private TextField keyword;
	@FXML
	private TextField notes;
	@FXML
	private Button save;
	@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	@FXML
	private Button back;
	@FXML
	void Select(ActionEvent event) {
		String s = vacancy.getSelectionModel().getSelectedItem().toString();

	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		ObservableList<String> list = FXCollections.observableArrayList("Associate IT Manager", "Junior Account Assistant", "Software Engineer");
		vacancy.setItems(list);
	}
	public void back(ActionEvent event) {
		new Candidates().show();
	}
	public void admin(ActionEvent event) {
		new User().show();
	}
	public void search(ActionEvent event) {
		new User().show();
	}

	public void recruitment(ActionEvent event) {
		new Recrutement().show();

	}

	public void dashboard(ActionEvent event) {
		new Dashboard().show();

	}

	public void save(ActionEvent event) {
		System.out.println(vacancy.getPromptText());
		System.out.println(first.getPromptText());
		System.out.println(last.getText());
		System.out.println(email.getText());
		System.out.println(number.getText());
		System.out.println(keyword.getText());
		System.out.println(notes.getText());
		

		String query = "update candidate set first_name ='" +first.getText() + "',last_name"
				+ " ='" + last.getText() + "',vacancy ='"
		+ vacancy.getValue() + "',email ='" + email.getText() + "',mobile_number ='" + number.getText() +
		"',keyword ='" + keyword.getText() + "',notes ='" + notes.getText() + "' where first_name='"
		+ candidateToEdit.getText() + "';";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur admin controller " + event.getEventType().getName());

		new Recrutement().show();

	}
}
