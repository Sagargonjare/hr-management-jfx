package editVacancy;

import java.net.URL;
import java.util.ResourceBundle;

import dashboard.Dashboard;
import dbOperation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import recrutement.Recrutement;
import searchVacancy.SearchVacancy;
import users.User;
import vacancies.Vacancies;

public class EditVacancyController implements Initializable{


	@FXML
	private ComboBox jobtitle;
	@FXML
	private ComboBox vacancy;
	@FXML
	private ComboBox manager;
	@FXML
	private ComboBox status;
	@FXML
	private Button add;
	@FXML
	private Button search;
	@FXML
	private Button dashboard;
	@FXML
	private Button recruitment;
	@FXML
	private Button admin;
	@FXML
	private Button back;
	@FXML
	private ComboBox vacancy1;
	public void back(ActionEvent event) {
		new Vacancies().show();
	}
	
	@FXML
	private TableView<SearchVacancy> tableView;
	@FXML
	private TableColumn<SearchVacancy,String> col1;
	@FXML
	private TableColumn<SearchVacancy,String> col2;
	@FXML
	private TableColumn<SearchVacancy,String> col3;
	@FXML
	private TableColumn<SearchVacancy,String> col4;
	@FXML
	void Select(ActionEvent event) {
		String s = jobtitle.getSelectionModel().getSelectedItem().toString();

	}
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		ObservableList<String> list = FXCollections.observableArrayList("Account Assistant", "Chief Exclutive", "Chief Financial","Databace Administrator");
		jobtitle.setItems(list);
		ObservableList<String> list2 = FXCollections.observableArrayList("Associate IT Manager", "Junior Account Assistant", "Senior QL Lead");
		vacancy.setItems(list2);
		ObservableList<String> list3 = FXCollections.observableArrayList("Gaurav Pandarkar", "Sanket Yelam", "Ram Shinde","Anuja Chavan");
		manager.setItems(list3);
		ObservableList<String> list4 = FXCollections.observableArrayList("Active",  "Close");
		status.setItems(list4);
		ObservableList<String> list5 = FXCollections.observableArrayList("Associate IT Manager", "Junior Account Assistant", "Senior QL Lead");
		vacancy1.setItems(list5);
	}

	

	public void admin(ActionEvent event) {

		new User().show();

	}
	public void search(ActionEvent event) {
		new User().show();

	}

	public void recruitment(ActionEvent event) {
		new Recrutement().show();

	}

	public void dashboard(ActionEvent event) {
		new Dashboard().show();

	}

	public void add(ActionEvent event) {
		System.out.println(jobtitle.getPromptText());
		System.out.println(vacancy.getPromptText());
		System.out.println(manager.getPromptText());
		System.out.println(status.getPromptText());
		/*String query1 = "insert into recruVacancy(jobtitle,vacency,manager,status) values ('"
				+ jobtitle.getValue() + "', '" + vacancy.getValue() + "','" + manager.getValue() + "','"
				+ status.getValue()  + "');";*/
		
		
		String query = "update recruVacancy set jobtitle ='" +jobtitle.getValue() + "',vacency"
				+ " ='" +  vacancy.getValue()  + "',manager ='"
		+  manager.getValue() + "',status ='" + status.getValue()+  "' where vacency='"
		+ vacancy1.getValue() + "';";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur admin controller " + event.getEventType().getName());

		new Recrutement().show();
	}
}
