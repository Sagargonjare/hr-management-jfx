package application;

import dbOperation.DbUtil;
import editVacancy.EditVacancy;
import javafx.application.Application;
import javafx.stage.Stage;
import login.Login;
import stageMaster.StageMaster;

public class ApplicationMain extends Application{
	public static void main(String args[]) {
		DbUtil.createDbConnection();
		launch(args);
		
	}
	public void start(Stage primaryStage) {
		StageMaster.setStage(primaryStage);
		new Login().show();
		
	}

}

