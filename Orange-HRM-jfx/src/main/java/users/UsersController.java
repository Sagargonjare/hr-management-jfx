package users;

import addUser.AddUser;
import dashboard.Dashboard;
import deleteUser.DeleteUser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import recrutement.Recrutement;
import searchUser.SearchUser;
import vacancies.Vacancies;

public class UsersController {
	@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	@FXML
	private Button adduser;
	@FXML
	private Button deleteuser;
	
	@FXML
	private Button back;

	public void back(ActionEvent event) {
		new Dashboard().show();
	}
	
	public void admin(ActionEvent event) {
		new  User().show();
	}
	public void recruitment(ActionEvent event) {
		new Recrutement().show();
	}
	public void dashboard(ActionEvent event ) {
		new Dashboard().show();
	}
	public void adduser(ActionEvent event) {
		new AddUser().show();
		
	}
	public void deleteuser(ActionEvent event) {
		new  DeleteUser().show();
	}
	
	public void searchuser(ActionEvent event) {
		new SearchUser().show();
		
	}

}
