package login;

import java.sql.Connection;

import com.mysql.cj.xdevapi.Statement;

import dashboard.Dashboard;
import dbOperation.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class LoginController {
	

	static Statement stmt;
	static Connection con;
	@FXML
	private TextField userName;
	@FXML
	private TextField password;
	@FXML
	private Button login;
	
	
	public void login(ActionEvent event) {

		String username = userName.getText();
		String Password = password.getText();
		if (username.equals("admin") && Password.equals("admin123")) {
			
			System.out.println("Event occur login controller " + event.getEventType().getName());
			new Dashboard().show();
		} else {
			System.out.println("Invalid login");

		}

	}

}
