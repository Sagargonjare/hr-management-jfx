package addUser;

import java.net.URL;
import java.util.ResourceBundle;

import dashboard.Dashboard;
import dbOperation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import recrutement.Recrutement;
import searchVacancy.SearchVacancy;
import users.User;

public class AddUserController implements Initializable {
	@FXML
	private ComboBox userRole;
	@FXML
	private ComboBox status;
	@FXML
	private TextField employeeName;
	@FXML
	private TextField userName;
	@FXML
	private PasswordField password;
	@FXML
	private PasswordField confirmPassword;
	@FXML
	private Button save;
	@FXML
	private Button search;
	@FXML
	private Button back;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	@FXML
	private TableView<User> tableView;
	@FXML
	private TableColumn<User,String> col1;
	@FXML
	private TableColumn<User,String> col2;
	@FXML
	private TableColumn<User,String> col3;
	@FXML
	private TableColumn<User,String> col4;
	@FXML
	private TableColumn<User,String> col5;
	
	@FXML
	void Select(ActionEvent event) {
		String s = userRole.getSelectionModel().getSelectedItem().toString();

	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		ObservableList<String> list = FXCollections.observableArrayList("Admin", "ESS");
		userRole.setItems(list);
		ObservableList<String> list2 = FXCollections.observableArrayList("Enable", "Disable");
		status.setItems(list2);
	}

	public void admin(ActionEvent event) {

		new User().show();

	}

	public void search(ActionEvent event) {

	}

	public void recruitment(ActionEvent event) {

	}

	public void dashboard(ActionEvent event) {
		new Dashboard().show();

	}

	public void cancel(ActionEvent event) {
		new User().show();

	}

	public void save(ActionEvent event) {
		System.out.println(userRole.getPromptText());
		System.out.println(status.getPromptText());
		System.out.println(employeeName.getText());
		System.out.println(userName.getText());
		System.out.println(password.getText());
		System.out.println(confirmPassword.getText());

		String query = "insert into admin(User_Role,status,Employee_Name,User_Name,Password,Confirm_Password) values ('"
				+ userRole.getValue() + "', '" + status.getValue() + "','" + employeeName.getText() + "','"
				+ userName.getText() + "','" + password.getText() + "','" + confirmPassword.getText() + "');";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur admin controller " + event.getEventType().getName());

		new User().show();

	}

}