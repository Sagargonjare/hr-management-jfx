package candidates;

import addCandidate.AddCandidate;
import addVacancy.AddVacancy;
import dashboard.Dashboard;
import deleteCandidate.DeleteCandidate;
import editCandidate.EditCandidate;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import recrutement.Recrutement;
import searchCandidate.SearchCandidate;
import users.User;

public class CandidatesController {
	@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	@FXML
	private Button back;

	public void back(ActionEvent event) {
		new Recrutement().show();
	}
	public void search(ActionEvent event) {
		new Dashboard().show();
		
	}
	public void edit(ActionEvent event) {
		new EditCandidate().show();
		
	}
	public void admin(ActionEvent event) {
		new  User().show();
	}
	public void recrutement(ActionEvent event) {
		new Recrutement().show();
	}
	public void dashboard(ActionEvent event ) {
		new Dashboard().show();
	}
	public void add(ActionEvent event ) {
		new AddCandidate().show();
	}
	public void delete(ActionEvent event ) {
		new DeleteCandidate().show();
		
	}public void searchv(ActionEvent event ) {
		new SearchCandidate().show();
	}


}
