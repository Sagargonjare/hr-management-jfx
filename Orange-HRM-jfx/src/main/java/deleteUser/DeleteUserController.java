package deleteUser;

import candidates.Candidates;
import dashboard.Dashboard;
import dbOperation.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import recrutement.Recrutement;
import users.User;
import vacancies.Vacancies;

public class DeleteUserController {
	@FXML
	private Button delete;
	@FXML
	private Button search;
	
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private TextField name;
	@FXML
	private Button back;

	public void back(ActionEvent event) {
		new User().show();
	}
	public void admin(ActionEvent event) {

		new User().show();

	}

	public void search(ActionEvent event) {
			new Dashboard().show();
	}

	public void recruitment(ActionEvent event) {
			new Recrutement().show();
	}

	public void dashboard(ActionEvent event) {
		new Dashboard().show();

	}

	public void delete(ActionEvent event) {
		System.out.println(name.getText());

		String query = " delete from admin where Employee_Name ='" + name.getText() + "';";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur delete controller " + event.getEventType().getName());
		new User().show();
	}
}
