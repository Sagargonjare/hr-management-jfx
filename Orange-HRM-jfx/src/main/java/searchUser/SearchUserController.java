package searchUser;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import candidates.Candidates;
import dashboard.Dashboard;
import dbOperation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import recrutement.Recrutement;
import searchVacancy.SearchVacancy;
import users.User;

public class SearchUserController  implements Initializable {
	@FXML
	private TextField userName;

	@FXML
	private Button search1;
	
	@FXML
	private Button cancel;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	
	@FXML
	private TableView<Admin> tableView;
	@FXML
	private TableColumn<Admin,String> col1;
	@FXML
	private TableColumn<Admin,String> col2;
	@FXML
	private TableColumn<Admin,String> col3;
	@FXML
	private TableColumn<Admin,String> col4;
	@FXML
	private TableColumn<Admin,String> col5;
	
	private ObservableList<Admin> data;

	@FXML
	private Button back;

	public void back(ActionEvent event) {
		new User().show();
	}

	public void admin(ActionEvent event) {
		new User().show();
	}

	public void recruitment(ActionEvent event) {
		new Recrutement().show();
	}

	public void dashboard(ActionEvent event) {
		new Dashboard().show();

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		data = FXCollections.observableArrayList();

		col1.setCellValueFactory(new PropertyValueFactory<Admin, String>("userRole"));
		col2.setCellValueFactory(new PropertyValueFactory<Admin, String>("employeeName"));
		col3.setCellValueFactory(new PropertyValueFactory<Admin, String>("status"));
		col4.setCellValueFactory(new PropertyValueFactory<Admin, String>("userName"));
		col5.setCellValueFactory(new PropertyValueFactory<Admin, String>("password"));

		buildData();

		FilteredList<Admin> filteredData = new FilteredList<>(data, b -> true);
		userName.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(Admin -> {
				if (newValue.isEmpty() || newValue.isBlank() || newValue == null) {
					return true;
				}
				String searchKeyword = newValue.toLowerCase();
				if (Admin.getUserRole().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (Admin.getEmployeeName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (Admin.getStatus().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (Admin.getUserName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (Admin.getPassword().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				
				} else
					return false;

			});
		});

		SortedList<Admin> sortedData = new SortedList<>(filteredData);

		sortedData.comparatorProperty().bind(tableView.comparatorProperty());
		tableView.setItems(sortedData);

	}

	public void buildData() {
		try {
			data = FXCollections.observableArrayList();
			String query = "Select*from admin";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				Admin users = new Admin();
				users.userRole.set(resultSet.getString(2));
				users.employeeName.set(resultSet.getString(3));
				users.status.set(resultSet.getString(4));
				users.userName.set(resultSet.getString(5));
				users.password.set(resultSet.getString(6));

				data.add(users);
			}
			tableView.setItems(data);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
