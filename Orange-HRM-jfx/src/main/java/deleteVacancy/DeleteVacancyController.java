package deleteVacancy;

import candidates.Candidates;
import dashboard.Dashboard;
import dbOperation.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import recrutement.Recrutement;
import users.User;
import vacancies.Vacancies;

public class DeleteVacancyController {
	@FXML
	private Button delete;
	@FXML
	private Button search;
	@FXML
	private Button cancel;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private TextField vacancy;
	@FXML
	private Button back;

	public void back(ActionEvent event) {
		new Candidates().show();
	}
	public void admin(ActionEvent event) {

		new User().show();

	}

	public void search(ActionEvent event) {

	}

	public void recruitment(ActionEvent event) {
		new Recrutement().show();

	}

	public void dashboard(ActionEvent event) {
		new Dashboard().show();

	}

	public void delete(ActionEvent event) {
		System.out.println(vacancy.getText());

		String query = " delete from recruVacancy where vacency ='" + vacancy.getText() + "';";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur delete controller " + event.getEventType().getName());
		new Candidates().show();
	}
}
