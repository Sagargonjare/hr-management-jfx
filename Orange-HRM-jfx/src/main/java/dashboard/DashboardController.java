package dashboard;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import recrutement.Recrutement;
import users.User;

public class DashboardController {
	@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button dashboard;
	
	public void search(ActionEvent event) {
		new Dashboard().show();
		
	}
	public void admin(ActionEvent event) {
		new  User().show();
	}
	public void recruitment(ActionEvent event) {
		new Recrutement().show();
	}
	public void dashboard(ActionEvent event ) {
		new Dashboard().show();
	}
}
